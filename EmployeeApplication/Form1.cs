﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EmployeNamespace;
namespace EmployeeApplication
{
    public partial class frmComputeSalary : Form
    {
      
        public frmComputeSalary()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //tb1 first
            //tb2 last
            //tb3 dept
            //tb4 job
            //tb6 hours
            //tb5 rate
            //lbl10 firstname out
            //lbl11 lastname out
            //lbl12 salary out
            string
                first_name = textBox1.Text,
                last_name = textBox2.Text,
                department = textBox3.Text,
                job_title = textBox4.Text;

            int hoursWorked = Convert.ToInt32(textBox6.Text); 
            double ratesPerHour = Convert.ToDouble(textBox5.Text);
            PartTimeEmployee partTimeEmployee = new PartTimeEmployee(first_name, last_name, department, job_title);
            partTimeEmployee.computeSalary(hoursWorked, ratesPerHour);
            label10.Text = partTimeEmployee.FirstName;
            label11.Text = partTimeEmployee.LastName;
            label12.Text = "\u20b1" + Convert.ToString(partTimeEmployee.BasicSalary);
        }
    }
}
